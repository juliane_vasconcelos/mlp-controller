package utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LeitorPesos {

	String g_nomeArquivoPesos;
	private BufferedReader _br;
	private double[][] _pesosInput;
	private double[][] _pesosOutput;
	private int g_tamanho;

	public LeitorPesos(String _nomeArquivoPesos) {
		g_nomeArquivoPesos = _nomeArquivoPesos;
		carregaPesos();
	}

	public void carregaPesos() {
		List<double[]> _linhasDouble = new ArrayList<double[]>();
		double[] _linhaPesos;
		_br = null;

		try {
			_br = new BufferedReader(new FileReader(g_nomeArquivoPesos));
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}

		// Carrega a matriz de pesos - input
		String _linha = null;
		try {
			while (!((_linha = _br.readLine()) == null || (_linha.equals("")))) {

				// Separacao por virgula
				String[] _l = _linha.split(",");
				_linhaPesos = new double[_l.length];

				for (int i = 0; i < _linhaPesos.length; i++) {

					// Para cada atributo, modifica para double
					_linhaPesos[i] = Double.parseDouble(_l[i]);
				}
				_linhasDouble.add(_linhaPesos);
			}
		} catch (IOException e1) {

			e1.printStackTrace();
		}

		g_tamanho = _linhasDouble.size();

		// Carrega na matriz input
		_pesosInput = new double[_linhasDouble.size()][g_tamanho];
		for (int i = 0; i < _pesosInput.length; i++) {

			_pesosInput[i] = _linhasDouble.remove(0).clone();
		}

		// Carrega a matriz de pesos - output
		try {
			while (!((_linha = _br.readLine()) == null || (_linha.equals("")))) {

				// Separacao por virgula
				String[] _l = _linha.split(",");
				_linhaPesos = new double[_l.length];

				for (int i = 0; i < _linhaPesos.length; i++) {

					// Para cada atributo, modifica para double
					_linhaPesos[i] = Double.parseDouble(_l[i]);
				}
				_linhasDouble.add(_linhaPesos);
			}
		} catch (IOException e2) {

			e2.printStackTrace();
		}

		// Carrega na matriz output
		_pesosOutput = new double[_linhasDouble.size()][];
		for (int i = 0; i < _pesosOutput.length; i++) {

			_pesosOutput[i] = _linhasDouble.remove(0);
		}

		try {
			_br.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public double[][] retornaPesosInput() {

		return _pesosInput;
	}

	public double[][] retornaPesosOutput() {

		return _pesosOutput;
	}
}