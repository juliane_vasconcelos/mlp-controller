package utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import preprocessamento.separacao_atributos_classes.objetos.Dado;

public class LeitorArquivoTeste {

	String g_nomeArquivoPrototipo;
	private BufferedReader _br;
	List<Dado> g_listaTeste;
	private double[] _atributos;

	public static void main(String[] args) {
		LeitorArquivoTeste leitor = new LeitorArquivoTeste();
		leitor.carregaDados("C:/Users/Felipe/Desktop/classes/IA/project/testResults/lvq/arquivosTreino/treinaLVQPesosRandomicosCom10NeuroniosAtributosPrototipoEpoca-300.txt");
	}

	public List<Dado> carregaDados(String nomeArquivo) {
		g_listaTeste = new ArrayList<Dado>();
		_br = null;
		try {
			_br = new BufferedReader(new FileReader(nomeArquivo));
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}

		// Leitura por linha e carregamento de um dado
		String _linha = null;
		try {
			while ((_linha = _br.readLine()) != null) {

				// Separacao por virgula
				String[] _l = _linha.split(";");
				_atributos = new double[_l.length - 1];

				int _ultima = _l.length - 1;
				for (int i = 0; i < _atributos.length; i++) {

					// Para cada atributo, modifica para double
					try {
						_atributos[i] = Double.parseDouble(_l[i].replace(",",
								"."));
					} catch (Exception e) {
						System.out.println(_l[i]);
						_atributos[i] = Double.parseDouble(_l[i]
								.substring(0, 1));
					}
				}
				int _classe = Integer.parseInt(_l[_ultima]);

				// Cria um dado com classe e atributos
				Dado _dado = new Dado(_classe, _atributos);
				g_listaTeste.add(_dado);
			}
		} catch (IOException e1) {

			e1.printStackTrace();
		}

		try {
			_br.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

		// Retorna lista
		return g_listaTeste;
	}
}