package facade;

import java.io.IOException;
import java.util.List;

import preprocessamento.separacao_atributos_classes.objetos.Dado;
import mlp.enumeration.WeightInitialization;

public interface MLPFacade {

	public void treinaMLP(double taxaDeAprendizado, //
			int epocas,//
			int frequencia,//
			double limite, //
			int nNeuroniosCamadaEntrada,//
			int nNeuroniosCamadaEscondida,//
			int nNeuroniosCamadaSaida,//
			WeightInitialization modoInicializacaoDePesos, //
			double[][] pesos1,//
			double[][] pesos2,//
			List<Dado> dadosTreinamento,//
			List<Dado> dadosValidacao) throws IOException;
	
	public void usaMlP(double taxaDeAprendizado, //
			int epocas,//
			int frequencia,//
			double limite, //
			int nNeuroniosCamadaEntrada,//
			int nNeuroniosCamadaEscondida,//
			int nNeuroniosCamadaSaida,//
			WeightInitialization modoInicializacaoDePesos, //
			String nomeArquivoPesos,//
			String nomeArquivoEntrada) throws IOException;
	
	public void treinaMLP(double taxaDeAprendizado, //
			int epocas,//
			int frequencia,//
			double limite, //
			int nNeuroniosCamadaEntrada,//
			int nNeuroniosCamadaEscondida,//
			int nNeuroniosCamadaSaida,//
			WeightInitialization modoInicializacaoDePesos, //
			double[][] pesos1,//
			double[][] pesos2,//
			String nomeArquivo) throws IOException;

}