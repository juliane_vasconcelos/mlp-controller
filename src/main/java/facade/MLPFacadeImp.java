package facade;

import holdout.Holdout;

import java.io.IOException;
import java.util.List;

import mlp.Mlp;
import mlp.enumeration.WeightInitialization;
import preprocessamento.separacao_atributos_classes.objetos.Dado;
import utils.LeitorArquivoTeste;
import utils.LeitorPesos;

public class MLPFacadeImp implements MLPFacade {

	public void treinaMLP(double taxaDeAprendizado, //
			int epocas,//
			int frequencia,//
			double limite, //
			int nNeuroniosCamadaEntrada,//
			int nNeuroniosCamadaEscondida,//
			int nNeuroniosCamadaSaida,//
			WeightInitialization modoInicializacaoDePesos, //
			double[][] pesos1,//
			double[][] pesos2,//
			List<Dado> dadosTreinamento,//
			List<Dado> dadosValidacao) throws IOException {
		Mlp mlp = new Mlp(taxaDeAprendizado, epocas, frequencia, limite,
				nNeuroniosCamadaEntrada, nNeuroniosCamadaEscondida,
				nNeuroniosCamadaSaida, modoInicializacaoDePesos, pesos1, pesos2);

		mlp.trainMLP(dadosTreinamento, dadosValidacao);
	}

	public void usaMlP(double taxaDeAprendizado, //
			int epocas,//
			int frequencia,//
			double limite, //
			int nNeuroniosCamadaEntrada,//
			int nNeuroniosCamadaEscondida,//
			int nNeuroniosCamadaSaida,//
			WeightInitialization modoInicializacaoDePesos, //
			String nomeArquivoPesos,//
			String nomeArquivoEntrada) throws IOException {

		LeitorArquivoTeste lat = new LeitorArquivoTeste();
		List<Dado> dadosTeste = lat.carregaDados(nomeArquivoEntrada);

		LeitorPesos lp = new LeitorPesos(nomeArquivoPesos);
		double[][] pesos1 = lp.retornaPesosInput();
		double[][] pesos2 = lp.retornaPesosOutput();
		Mlp mlp = new Mlp(taxaDeAprendizado, epocas, frequencia, limite,
				nNeuroniosCamadaEntrada, pesos2.length, nNeuroniosCamadaSaida,
				modoInicializacaoDePesos, pesos1, pesos2);

		mlp.useMLP(dadosTeste, pesos1, pesos2);
	}

	public void treinaMLP(double taxaDeAprendizado, //
			int epocas,//
			int frequencia,//
			double limite, //
			int nNeuroniosCamadaEntrada,//
			int nNeuroniosCamadaEscondida,//
			int nNeuroniosCamadaSaida,//
			WeightInitialization modoInicializacaoDePesos, //
			double[][] pesos1,//
			double[][] pesos2,//
			String nomeArquivo) throws IOException {
		Holdout holdout = new Holdout(nomeArquivo);

		List<Dado> baseDadosTreinamento = holdout.getListaTreinamento();
		List<Dado> baseDadosValidacao = holdout.getListaValidacao();

		holdout.criaArquivoTeste();

		treinaMLP(taxaDeAprendizado, epocas, frequencia, limite,
				nNeuroniosCamadaEntrada, nNeuroniosCamadaEscondida,
				nNeuroniosCamadaSaida, modoInicializacaoDePesos, pesos1,
				pesos2, baseDadosTreinamento, baseDadosValidacao);
	}

}