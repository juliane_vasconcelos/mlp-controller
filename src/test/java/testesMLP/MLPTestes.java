package testesMLP;

import holdout.Holdout;

import java.io.IOException;
import java.util.List;

import mlp.enumeration.WeightInitialization;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import preprocessamento.separacao_atributos_classes.objetos.Dado;
import utils.LeitorPesos;
import categoria.Teste10Neuronios;
import facade.MLPFacade;
import facade.MLPFacadeImp;

public class MLPTestes {

	private List<Dado> listaTreinamento;
	private List<Dado> listaValidacao;
	private double taxaDeAprendizado;
	private int epocas;
	private int frequencia;
	private double limite;
	private int nNeuroniosCamadaEntrada;
	private int nNeuroniosCamadaEscondida;
	private int nNeuroniosCamadaSaida;
	private WeightInitialization modoInicializacaoDePesos;
	private double[][] pesos1;
	private double[][] pesos2;

	private final int CINCO_NEURONIOS = 5;
	private final int DEZ_NEURONIOS = 10;
	private final int QUINZE_NEURONIOS = 15;
	private final double MAX_TAXA_APRENDIZADO = 0.5;

	MLPFacade mlpFacade;

	@Before
	public void before() {
		mlpFacade = new MLPFacadeImp();

		Holdout holdout = new Holdout();
		listaTreinamento = holdout.getListaTreinamento();
		listaValidacao = holdout.getListaValidacao();

		taxaDeAprendizado = 0.3;
		epocas = 10000;
		frequencia = 5;
		limite = 0.0001;
		nNeuroniosCamadaEntrada = 57;
		nNeuroniosCamadaEscondida = 1;
		nNeuroniosCamadaSaida = 4;
		pesos1 = null;
		pesos2 = null;

	}
	
	@Category(Teste10Neuronios.class)
	@Test
	public void treinaMLPPesosRandomicosCom10NeuroniosTaxaAprendizadoMaior()
			throws IOException {
		modoInicializacaoDePesos = WeightInitialization.RANDOM;
		nNeuroniosCamadaEscondida = DEZ_NEURONIOS;
		taxaDeAprendizado = MAX_TAXA_APRENDIZADO;
		
		Holdout holdout = new Holdout();
		LeitorPesos lp = new LeitorPesos("C:/Users/Felipe/Desktop/classes/IA/project/testResults/mlp/10neuronios/treinaMLPPesosRandomicosCom10NeuroniosTaxaAprendizadoMaiorPesosEpoca-5660.txt");
		
		String nomePeso = "";
		double[][] weights1 = lp.retornaPesosInput();
		double[][] weights2 = lp.retornaPesosOutput();
		String nomeEntrada = "";

		
		mlpFacade.usaMlP(taxaDeAprendizado, epocas, frequencia, limite,
				nNeuroniosCamadaEntrada, nNeuroniosCamadaEscondida,
				nNeuroniosCamadaSaida, modoInicializacaoDePesos, nomeEntrada,nomePeso);
	}
}
