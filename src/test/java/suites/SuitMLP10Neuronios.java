package suites;

import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import testesMLP.MLPTestes;
import categoria.Teste10Neuronios;

@RunWith(Categories.class)
@IncludeCategory(Teste10Neuronios.class)
@Suite.SuiteClasses(MLPTestes.class)
public class SuitMLP10Neuronios {

}
