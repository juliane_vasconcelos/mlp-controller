package suites;

import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import testesMLP.MLPTestes;
import categoria.Teste5Neuronios;

@RunWith(Categories.class)
@IncludeCategory(Teste5Neuronios.class)
@Suite.SuiteClasses(MLPTestes.class)
public class SuitMLP5Neuronios {

}
